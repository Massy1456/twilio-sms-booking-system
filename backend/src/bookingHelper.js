const matchType = (req, messageContent) => {
    
    if(messageContent.includes('gym')){
        req.session.type = 'gym'
    } else if(messageContent.includes('trainer')) {
        req.session.type = 'personal trainer'
    } else if(messageContent.includes('massage')){
        req.session.type = 'masseur'
    }

    if(!req.session.type){
        return `Sorry, I didn't understand your request`
    } 

    req.session.step = 2 // we will increase the step if the user entered the qualified info
    return `What date do you want to see the ${req.session.type}?`
}

const matchDay = (req, messageContent) => {
    const weekdays = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
    const day = weekdays.filter((w) => messageContent.toLowerCase().includes(w))
    
    if(day.length === 0){ // if the users didn't enter a weekday, we reprompt them
        return `Sorry, an invalid week day was entered. Please try again`
    } else if(day.length > 1){
        return `Sorry, please enter only one day of the week for your bookings`
    }

    req.session.step = 3
    req.session.weekday = day
    return `Great! What time do you want to have your ${day} appointment?:\n
                11:00am, 1:00pm, 3:00pm, 4:00pm`
}

const matchHour = (req, messageContent) => {
    const hourRegex = /\b(\d?\d)\s?([aApP][mM])/g;
    const hour = hourRegex.exec(messageContent) // use regex to make sure time is correct format
    
    if(hour && hour.length === 3){
        const {type, weekday} = req.session // we deconstruct our session to get our appointment type and chosen day
        req.session.step = 4
        req.session.time = hour[0] // we store our session time
        return `Your appointment to see the ${type} on ${weekday} at ${hour[0]} was set. Thank you!`
    }

    return `Sorry, we could not understand what time you want to set your appointment. Please try again.`
}

const confirmApp = (req) => {
    const {type, weekday, time} = req.session
    return `Your appointment to see the ${type} on ${weekday} at ${time} was set.\nLet us know if you need to update your appointment.`
}

module.exports = {
    matchType,
    matchDay,
    matchHour,
    confirmApp
}