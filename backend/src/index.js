
const {matchType, matchDay, matchHour, confirmApp} = require('./bookingHelper')


require('dotenv').config() // initialize the env variables
// this is the same as const express = require('express') THEN const app = express()
const bodyParser = require('body-parser')
const session = require('express-session')
const messagingResponse = require('twilio').twiml.MessagingResponse // twiml is an xml parser that twilio created
const app = require('express')()

app.use(bodyParser.urlencoded({extended: true}))
app.use(session({secret: process.env.SESSION_SECRET}))
const port = process.env.PORT || 3001

app.get('/test', (req, res) => {
    res.send('Hello from server')
})

// whenever a message is sent to our fake number, 
// twilio is notified and a response is fired
// with the senders information
app.post('/recieve-sms', (req, res) => {
    const messageContent = req.body.Body.toLowerCase() // the content of the message
    const step = req.session.step
    let message;
    if(!step){
        req.session.step = 1
        message = `Hello, do you want to book an appointment to: \n
        see the gym \n
        book a personal trainer \n
        book a massage`;
    } else {
        switch(step){
            // each case of the switch statement is a step in the process 
            case 1:
                // checks to see what the user texted back, if anything sent to console
                message = matchType(req, messageContent) // from our bookng helper
                break
            case 2:
                // filter the message and see if it includes one of the days of the week
                message = matchDay(req, messageContent)
                break
            case 3:
                // user enters hour for appointment, make sure its valid
                message = matchHour(req, messageContent)
                break
            case 4:
                // confirmation of appointment
                message = confirmApp(req)
                break
            default:
                console.log(`could not find for ${step}`)
        }
    }
    
    const twiml = new messagingResponse() // create a new twiml response
    twiml.message(message) // send the message
    console.log(twiml.toString())
    res.set('Content-Type', 'text/xml')
    res.send(twiml.toString())
})

app.listen(port, () => {
    console.log(`Listening on port ${port}`)
})

