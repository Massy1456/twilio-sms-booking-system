const twilio = require('twilio')(
    process.env.TOKEN_SID,
    process.env.TOKEN_SECRET,
    {
        accountSid: process.env.ACCOUNT_SID
    }
)

const from = process.env.PHONE_NUMBER
const to = process.env.MY_NUMBER

// simple way to send a message from your twilio phone number
const sendMessage = async () => {
    try {
        const response = await twilio.messages.create({
            from,
            to,
            body: 'Hello from Twilio'
        })
        console.log(`message sent from ${response.from} to ${response.to}`)
    } catch (e) {
        console.error(e)
    }

}

sendMessage()